function capturarData() {
    let lugarOrigen = document.getElementById("Origen").value;
    let lugarDestino = document.getElementById("Destino").value;
    let fechaInicio = document.getElementById("fechaInicio").value;
    let fechaFin = document.getElementById("fechaFin").value;

    localStorage.setItem("Origen", lugarOrigen);
    localStorage.setItem("Destino", lugarDestino);
    localStorage.setItem("fechaInicio", fechaInicio);
    localStorage.setItem("fechaFin", fechaFin);

}

function cargarData() {
    let lugarOrigen = localStorage.getItem("Origen");
    let lugarDestino = localStorage.getItem("Destino");
    let fechaInicio = localStorage.getItem("fechaInicio");
    let fechaFin = localStorage.getItem("fechaFin");

    document.getElementById("Origen").value = lugarOrigen;
    document.getElementById("Destino").value = lugarDestino;
    document.getElementById("fechaInicio").value = fechaInicio;
    document.getElementById("fechaFin").value = fechaFin;
}

function guardarUsuarios() {

    /*Cargar usuario existentes en memoria*/
    let users = [];
    users = JSON.parse(window.localStorage.getItem("users"));

    let mail = document.getElementById("email").value;
    let pwd = document.getElementById("pwd").value;
    let name = document.getElementById("name").value;

    let id = Object.keys(users).length;

    users.push({
        id: id + 1,
        mail: mail,
        name: name,
        pwd: pwd
    })

    localStorage.setItem("users", JSON.stringify(users));

    alert("Usario registrado exitosamente");
    window.location.href = "./rides.html";

}

function validarLogin() {

    let users = [];
    users = JSON.parse(window.localStorage.getItem("users"));

    let mail = document.getElementById("email").value;
    let pwd = document.getElementById("pwd").value;

    for (let index = 0; index <= users.length; index++) { //recorre el diccionario
        if (users[index].mail == mail) { //cuando encuentra el email
            if (users[index].pwd == pwd) {
                alert("Contraseña Correcta");
                sessionStorage.setItem("loggedUserId", users[index].id);
                window.location.href = "./rides.html";
            } else {
                alert("Contraseña Incorrecta");
            }
        }
    }
}

function CerrarSesion() {
    sessionStorage.setItem("loggedUserId", null);
}

function cargarDataPerfil() {
    /*Identifica el id del usuario logeado*/
    let idUsuario = null;
    idUsuario = sessionStorage.getItem('loggedUserId');

    if (idUsuario != null && idUsuario != "null") {
        /*ahora con el id, va al diccionario y se trae los datos del mae*/
        let users = [];
        users = JSON.parse(window.localStorage.getItem("users"));
        document.getElementById("name").value = users[idUsuario - 1].name;
        document.getElementById("email").value = users[idUsuario - 1].mail;
    } else {
        alert("Inicie sesión");
        window.location.href = "./login.html";
    }

}

function actualizarDataPerfil() {
    /*Identifica el id del usuario logeado*/
    let idUsuario = null;
    idUsuario = sessionStorage.getItem('loggedUserId') - 1;

    if (idUsuario != null && idUsuario != "null") {
        /*ahora con el id, va al diccionario y se trae los datos del mae*/
        let users = [];
        users = JSON.parse(window.localStorage.getItem("users"));

        /*actualiza los datos en el dic*/
        users[idUsuario].name = document.getElementById("name").value;
        users[idUsuario].mail = document.getElementById("email").value;
        users[idUsuario].pwd = document.getElementById("pwd").value;

        /*actualiza la info en localStorage*/
        localStorage.setItem("users", JSON.stringify(users));
        alert("Usuario actualizado");
        window.location.href = "./index.html";
    }
}

function crearAdmin() {

    let users2 = [];

    users2 = JSON.parse(window.localStorage.getItem("users")); /*si esta vacio es null*/

    if (users2 == null) { /* si no jay datos cargados previamente*/
        /*crea el admin*/
        let users = [{
            id: 1,
            mail: 'joseq2408@gmail.com',
            name: 'Jose Arturo Quiros',
            pwd: '12345'
        }];
        /*lo gaurda en localStorage*/
        localStorage.setItem("users", JSON.stringify(users));
        alert("Usuario admin creado");
    } /*de lo contrario se guardaria*/

}

function crearRides() {
    let rides2 = [];
    rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/

    if (rides2 == null) { /* si no jay datos cargados previamente*/
        /*crea el admin*/
        let rides = [{
            idRide: 1,
            idUsuario: 1,
            foto: 'img/avellanas.jpg',
            nombre: 'Playa Avellanas, Guanacaste',
            ubicacion: 'Guanacaste, Liberia',
            costo: 50,
            personas: 2,
            inicio: "07-07-2018",
            fin: "07-07-2018"
        }];
        /*lo gaurda en localStorage*/
        localStorage.setItem("rides", JSON.stringify(rides));
    } /*de lo contrario se guardaria*/
}

function reservarRide1() {
    /*primero valida que haya iniciado sesión*/
    /*Identifica el id del usuario logeado*/
    let idUsuario = null;
    idUsuario = sessionStorage.getItem('loggedUserId');

    if (idUsuario != null && idUsuario != "null") {
        /*ahora agrega el nuevo ride al dic de viajes*/
        /*crear el nuevo ride*/
        let nombreR = document.getElementById("nombreRide1").innerHTML;
        let ubiR = document.getElementById("ubiRide1").innerHTML;
        let desR = document.getElementById("desRide1").innerHTML;
        let costR = document.getElementById("costoRide1").innerHTML;
        let iniR = document.getElementById("fechaInicio").value;
        let finR = document.getElementById("fechaFin").value;

        /*importa el dic*/
        let rides2 = [];
        rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/

        let id = Math.floor(Math.random() * 10000) + 1;

        /*los añadimos al dic */
        rides2.push({
            idRide: id,
            idUsuario: idUsuario,
            foto: 'img/avellanas.jpg',
            nombre: nombreR,
            ubicacion: ubiR,
            costo: costR,
            personas: 2,
            inicio: iniR,
            fin: finR
        });

        localStorage.setItem("rides", JSON.stringify(rides2));
        alert("Ride reservado!");

    } else {
        alert("Inicie sesión");
        window.location.href = "./login.html";
    }

}

function reservarRide2() {
    /*primero valida que haya iniciado sesión*/
    /*Identifica el id del usuario logeado*/
    let idUsuario = null;
    idUsuario = sessionStorage.getItem('loggedUserId');

    if (idUsuario != null && idUsuario != "null") {
        /*ahora agrega el nuevo ride al dic de viajes*/
        /*crear el nuevo ride*/
        let nombreR = document.getElementById("nombreRide2").innerHTML;
        let ubiR = document.getElementById("ubiRide2").innerHTML;
        let desR = document.getElementById("desRide2").innerHTML;
        let costR = document.getElementById("costoRide2").innerHTML;
        let iniR = document.getElementById("fechaInicio").value;
        let finR = document.getElementById("fechaFin").value;


        /*importa el dic*/
        let rides2 = [];
        rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/

        let id = Math.floor(Math.random() * 10000) + 1;

        /*los añadimos al dic */
        rides2.push({
            idRide: id,
            idUsuario: idUsuario,
            foto: 'img/arenal2.jpg',
            nombre: nombreR,
            ubicacion: ubiR,
            costo: costR,
            personas: 2,
            inicio: iniR,
            fin: finR
        });

        localStorage.setItem("rides", JSON.stringify(rides2));
        alert("Ride reservado!");

    } else {
        alert("Inicie sesión");
        window.location.href = "./login.html";
    }

}

function reservarRide3() {
    /*primero valida que haya iniciado sesión*/
    /*Identifica el id del usuario logeado*/
    let idUsuario = null;
    idUsuario = sessionStorage.getItem('loggedUserId');

    if (idUsuario != null && idUsuario != "null") {
        /*ahora agrega el nuevo ride al dic de viajes*/
        /*crear el nuevo ride*/
        let nombreR = document.getElementById("nombreRide3").innerHTML;
        let ubiR = document.getElementById("ubiRide3").innerHTML;
        let desR = document.getElementById("desRide3").innerHTML;
        let costR = document.getElementById("costoRide3").innerHTML;
        let iniR = document.getElementById("fechaInicio").value;
        let finR = document.getElementById("fechaFin").value;

        /*importa el dic*/
        let rides2 = [];
        rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/

        let id = Math.floor(Math.random() * 10000) + 1;

        /*los añadimos al dic */
        rides2.push({
            idRide: id,
            idUsuario: idUsuario,
            foto: 'img/Limon.jpg',
            nombre: nombreR,
            ubicacion: ubiR,
            costo: costR,
            personas: 2,
            inicio: iniR,
            fin: finR
        });

        localStorage.setItem("rides", JSON.stringify(rides2));
        alert("Ride reservado!");

    } else {
        alert("Inicie sesión");
        window.location.href = "./login.html";
    }

}

function reservarRide4() {
    /*primero valida que haya iniciado sesión*/
    /*Identifica el id del usuario logeado*/
    let idUsuario = null;
    idUsuario = sessionStorage.getItem('loggedUserId');

    if (idUsuario != null && idUsuario != "null") {
        /*ahora agrega el nuevo ride al dic de viajes*/
        /*crear el nuevo ride*/
        let nombreR = document.getElementById("nombreRide4").innerHTML;
        let ubiR = document.getElementById("ubiRide4").innerHTML;
        let desR = document.getElementById("desRide4").innerHTML;
        let costR = document.getElementById("costoRide4").innerHTML;
        let iniR = document.getElementById("fechaInicio").value;
        let finR = document.getElementById("fechaFin").value;

        /*importa el dic*/
        let rides2 = [];
        rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/

        let id = Math.floor(Math.random() * 10000) + 1;

        /*los añadimos al dic */
        rides2.push({
            idRide: id,
            idUsuario: idUsuario,
            foto: 'img/Cartago.jpg',
            nombre: nombreR,
            ubicacion: ubiR,
            costo: costR,
            personas: 2,
            inicio: iniR,
            fin: finR
        });

        localStorage.setItem("rides", JSON.stringify(rides2));
        alert("Ride reservado!");

    } else {
        alert("Inicie sesión");
        window.location.href = "./login.html";
    }

}

function cargarRides() {

    let idUsuario = null;
    idUsuario = sessionStorage.getItem('loggedUserId');

    let rides2 = [];
    rides2 = JSON.parse(window.localStorage.getItem("rides"));

    if (idUsuario != null && idUsuario != "null") {

        for (let index = 0; index <= rides2.length; index++) { //recorre el diccionario
            if (rides2[index].idUsuario == idUsuario) { //cuando encuentra el id

                $("#DivRidesBackgroud").append(
                    '<div class="row, rowItemRide" style="display: inline-block;">' +
                    ' <article>' +
                    ' <div class="divLogoRide, col-sm-5">' +
                    '<img src="' + rides2[index].foto +
                    '" alt="Guanacaste" class="imgRide">' +
                    '</div>' +
                    '<div class="divDatosRide, col-sm-7">' +
                    '<h1 class="NombreRide">' + rides2[index].nombre + '</h1>' +
                    '<h2 class="UbicacionRide">' + rides2[index].ubicacion + '</h2>' +
                    '<h2>Costo por Persona ($): </h2>' +
                    '<h2>' + rides2[index].costo + ' </h2>' +
                    '<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>' +
                    '<a onclick="verRide(' + rides2[index].idRide + ')" href="./detail.html" class="btn btn-success">Ver Detalle </a>' +
                    '<a onclick="editarRide(' + rides2[index].idRide + ')" href="./detail.html" class="btn btn-primary">Modificar </a>' +
                    '<a onclick="eliminarRide(' + rides2[index].idRide + ')" class="btn btn-danger">Eliminar </a>' +
                    '</div>' +
                    '</article>' +
                    '</div>'
                );
            }
        }


    } else {
        alert("Inicie sesión");
        window.location.href = "./login.html";
    }
}

function eliminarRide(pIdRide) {

    let rides2 = [];
    rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/

    var r = confirm("Está seguro que desea eliminar este ride?");
    if (r == true) {

        var removeIndex = rides2.map(item => item.idRide)
            .indexOf(pIdRide);
        ~removeIndex && rides2.splice(removeIndex, 1);

        alert("Ride eliminado");
        localStorage.setItem("rides", JSON.stringify(rides2));
        window.location.href = "./mirdes.html";

    }
}

function editarRide(pIDRide) {
    sessionStorage.setItem("activeRideID", pIDRide);
    sessionStorage.setItem("edit", 1);
}

function verRide(pIDRide) {
    sessionStorage.setItem("activeRideID", pIDRide);
    sessionStorage.setItem("edit", 0);
}

function cargarDetalleRide() {
    let rides2 = [];
    let index = 0;
    let isEdit = null;

    isEdit = sessionStorage.getItem("edit");
    if (isEdit == 0) {
        document.getElementById("btnGuardar").style.display = "none";
        document.getElementById("cantPersonas").style.contenteditable = "false";
    }

    rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/

    pIdRide = sessionStorage.getItem("activeRideID");
    index = rides2.findIndex(x => x.idRide == pIdRide);

    document.getElementById("nomRide").innerHTML = rides2[index].nombre;
    document.getElementById("ubiRide").innerHTML = rides2[index].ubicacion;
    document.getElementById("costoRide").innerHTML = rides2[index].costo;
    document.getElementById("cantPersonas").innerHTML = rides2[index].personas;
    document.getElementById("fechaInicio").value = rides2[index].inicio;
    document.getElementById("fechaFin").value = rides2[index].fin;
    document.getElementById("imgRide").src = rides2[index].foto;
}

function actualizarRide() {

    let rides2 = [];
    let removeIndex = 0;

    rides2 = JSON.parse(window.localStorage.getItem("rides")); /*si esta vacio es null*/
    pIdRide = sessionStorage.getItem("activeRideID");
    removeIndex = rides2.findIndex(x => x.idRide == pIdRide);

    alert("el id" + pIdRide + " tiene el index " + removeIndex);

    rides2[removeIndex].inicio = document.getElementById("fechaInicio").value;
    rides2[removeIndex].fin = document.getElementById("fechaFin").value;
    rides2[removeIndex].personas = document.getElementById("cantPersonas").innerHTML;

    alert("Ride actualizado");
    localStorage.setItem("rides", JSON.stringify(rides2));

    window.location.href = "./mirdes.html";

}